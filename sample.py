class Car:

    # instance attribute
    def __init__(self, name, price):
        self.name = name
        self.price = price

# instantiate the Parrot class
car1 = Car("Audi", "10L")
car2 = Car("BMW", "15L")

print("{} is on road prices is {}".format( car1.name, car1.price))
print("{} is on road prices is {}".format( car2.name, car2.price))
