class BMW:

    def name(self):
        print("BMW")
    
    def price(self):
        print("15L")

class Audi:

    def name(self):
        print("Audi")
    
    def price(self):
        print("10L")

#instantiate objects
obj1 = BMW()
obj2 = Audi()

# passing the object
obj1.name
obj2.name
